package saetabis.automation.testingWrapper;

import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import saetabis.automation.testingWrapper.config.MainConfig;
import saetabis.automation.testingWrapper.config.PagesConfig;
import saetabis.automation.testingWrapper.models.UpgradeUser;
import saetabis.automation.testingWrapper.services.ui.driver.Browser;
import saetabis.automation.testingWrapper.services.ui.driver.DriverManager;
import saetabis.automation.testingWrapper.services.ui.driver.DriverManagerFactory;
import saetabis.automation.testingWrapper.services.ui.pages.HomePage;
import saetabis.automation.testingWrapper.services.ui.pages.LoginPage;
import saetabis.automation.testingWrapper.services.ui.pages.OfferPage;
import saetabis.automation.testingWrapper.services.ui.pages.RegisterPage;
import saetabis.automation.testingWrapper.utils.DataReader;

import java.util.Random;

@SpringBootTest(classes = {MainConfig.class})
public class uiTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private DriverManagerFactory driverManagerFactory;

    @Autowired
    private PagesConfig pagesConfig;

    @Autowired
    private DataReader dataReader;

    private DriverManager driverManager;

    private WebDriver driver;

    private SoftAssertions softly;

    @BeforeClass
    public void beforeClass () {

        driverManager = driverManagerFactory.getManager(Browser.CH);
        softly = new SoftAssertions ();
    }

    @AfterClass
    public void afterClass (){

       driver.close();
    }

    @Test
    public void registerUser() {

        UpgradeUser user = (UpgradeUser) dataReader.read(UpgradeUser.class, "users/nonRegisteredUser.json");
        String finalEmail = user.getEmail().replace("<", String.valueOf(new Random().nextInt(999)));
        user.setEmail(finalEmail);

        driver = driverManager.getWebDriver();

        HomePage home = new HomePage(driver, pagesConfig);

        RegisterPage registerPage = home.checkYourRate(user);

        OfferPage offerPage = registerPage.registerUser(user);

        String offeredLoanAmmount = offerPage.getLoanAmmount();
        String offeredAPR = offerPage.getAPR();
        String offeredLoanTerm = offerPage.getLoanTerm();
        String offeredMonthlyPayment = offerPage.getMonthlyPayment();

        offerPage.logOff();

        LoginPage loginPage = new LoginPage( driver, pagesConfig);
        loginPage.login(user);

        offerPage = new OfferPage(driver, pagesConfig);

        softly.assertThat(offerPage.getLoanAmmount()).isEqualTo(offeredLoanAmmount);
        softly.assertThat(offerPage.getAPR()).isEqualTo(offeredAPR);
        softly.assertThat(offerPage.getLoanTerm()).isEqualTo(offeredLoanTerm);
        softly.assertThat(offerPage.getMonthlyPayment()).isEqualTo(offeredMonthlyPayment);
    }
}