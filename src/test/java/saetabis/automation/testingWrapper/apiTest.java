package saetabis.automation.testingWrapper;

import org.apache.http.HttpStatus;
import org.assertj.core.api.SoftAssertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import saetabis.automation.testingWrapper.config.MainConfig;
import saetabis.automation.testingWrapper.services.api.LoanEndpoint;
import saetabis.automation.testingWrapper.services.api.responses.State;
import saetabis.automation.testingWrapper.services.api.responses.StatesResponse;

import io.restassured.response.Response;
import saetabis.automation.testingWrapper.utils.DataReader;

import java.util.List;
import java.util.stream.Collectors;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@SpringBootTest(classes = {MainConfig.class})
public class apiTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private LoanEndpoint loanEndPoint;

    @Autowired
    private DataReader dataReader;

    private SoftAssertions softly;

    @BeforeClass
    public void beforeClass()
    {
        softly = new SoftAssertions ();
    }

    @Test
    public void testStatesEndpoint () {

        Response response = loanEndPoint.getStates(HttpStatus.SC_OK);
        StatesResponse statesResponse = response.as(StatesResponse.class);
        List<State> states = statesResponse.getStates();

        Reporter.log("Making soft assertions",true);
        softly.assertThat(states.size()).isEqualTo(48);

        List<String> correctStatesLabels = dataReader.readStringArray("states/correctLabels.json");
        softly.assertThat(states.stream().map(state -> state.getLabel())
                .collect(Collectors.toList())).hasSameElementsAs(correctStatesLabels);

        State minAgeState = states.stream()
            .filter(filteredState -> filteredState.getMinAge().equals(19))
            .collect(Collectors.collectingAndThen(Collectors.toList(),
                list -> {
                    softly.assertThat(list.size()).isEqualTo(1);
                    return list.get(0);
                }));
        Reporter.log("State with 19 years old minimun age is: " + minAgeState.getLabel(), true);

        State georgiaState = states.stream()
            .filter(filteredState -> filteredState.getMinLoanAmount().equals(Long.valueOf(3005)))
            .collect(Collectors.collectingAndThen(Collectors.toList(),
                list -> {
                        softly.assertThat(list.size()).isEqualTo(1);
                        return list.get(0);
                }));
        softly.assertThat(georgiaState.getLabel()).isEqualTo("Georgia");

        softly.assertAll();
    }
 }
