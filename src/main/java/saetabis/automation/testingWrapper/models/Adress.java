package saetabis.automation.testingWrapper.models;

import lombok.Data;

@Data
public class Adress {

    private String homeAdress;

    private String city;

    private String state;

    private String zipCode;

    private String completeAdress;

    private String fullAdress;
}
