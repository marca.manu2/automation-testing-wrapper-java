package saetabis.automation.testingWrapper.models;

import lombok.Data;

@Data
public class UpgradeUser implements IDataTestModel {

    private String firstName;

    private String lastName;

    private Adress adress;

    private String dayOfBithday;

    private Long annualIncome;

    private Long additionalIncome;

    private String email;

    private String password;

    private Loan loan;
}
