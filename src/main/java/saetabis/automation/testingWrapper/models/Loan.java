package saetabis.automation.testingWrapper.models;

import lombok.Data;

@Data
public class Loan {

    private String purpose;

    private Long amount;

    private Long term;

    private Long monthlyPayment;
}
