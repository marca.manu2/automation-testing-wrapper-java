package saetabis.automation.testingWrapper.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix="endpoints")
public class EndpointsConfig {

    private String credifyUrl;

    private String port;

    private String loanEndpoint;
}
