package saetabis.automation.testingWrapper.services.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Reporter;
import saetabis.automation.testingWrapper.config.PagesConfig;
import saetabis.automation.testingWrapper.models.UpgradeUser;
import saetabis.automation.testingWrapper.services.ui.elements.LoginElements;

public class LoginPage extends Page {

    private LoginElements loginElements;

    public LoginPage(WebDriver _driver, PagesConfig _pagesConfig) {

        super(_driver, _pagesConfig.getBaseUrl(), _pagesConfig.getLoginPath());

        pagesConfig = _pagesConfig;
        loginElements = new LoginElements(30, _driver);
    }

    public void login(UpgradeUser user) {
            Reporter.log("REGISTERING USER",true);
            Reporter.log(dataReader.readObjToJsonString(user),true);
            fluentWait.until(ExpectedConditions.elementToBeClickable(loginElements.usernameInput));
            loginElements.usernameInput.sendKeys(user.getEmail());
            loginElements.passwordInput.sendKeys(user.getPassword());
            loginElements.signInButton.click();

        waitForLoading();

    }
}
