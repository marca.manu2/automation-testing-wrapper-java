package saetabis.automation.testingWrapper.services.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import saetabis.automation.testingWrapper.config.PagesConfig;
import saetabis.automation.testingWrapper.services.ui.elements.LogoutElements;

public class LogoutPage extends Page {

    private LogoutElements logoutElements;

    public LogoutPage(WebDriver _driver, PagesConfig _pagesConfig) {

        super(_driver, _pagesConfig.getBaseUrl(), _pagesConfig.getLogoutPath());

        pagesConfig = _pagesConfig;
        logoutElements = new LogoutElements(30, _driver);

        fluentWait.until(ExpectedConditions.elementToBeClickable(logoutElements.upgradeLogo));
    }



}
