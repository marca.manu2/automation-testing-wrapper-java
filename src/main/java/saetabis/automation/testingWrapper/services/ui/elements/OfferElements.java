package saetabis.automation.testingWrapper.services.ui.elements;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class OfferElements extends HeaderElements{

    public OfferElements(Integer _timeout, WebDriver _driver) {
        super(_timeout, _driver);
    }

    @FindBy(css="span[data-auto=\"userLoanAmount\"]")
    public WebElement loanAmount;

    @FindBy(css="span[data-auto=\"defaultMonthlyPayment\"]")
    public WebElement monthlyPayment;

    @FindBy(css="div[data-auto=\"defaultLoanTerm\"]")
    public WebElement loanTerm;

    @FindBy(css="div[data-auto=\"defaultLoanInterestRate\"]")
    public WebElement interestRate;

    @FindBy(css="div[data-auto=\"defaultMoreInfoAPR\"]>div")
    public WebElement apr;
}
