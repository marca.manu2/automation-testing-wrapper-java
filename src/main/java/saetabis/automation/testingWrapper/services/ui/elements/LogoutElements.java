package saetabis.automation.testingWrapper.services.ui.elements;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LogoutElements extends Elements{

    public LogoutElements(Integer _timeout, WebDriver _driver) {
        super(_timeout, _driver);
    }

    @FindBy(css="a[class=\"header__logo\"]")
    public WebElement upgradeLogo;
}
