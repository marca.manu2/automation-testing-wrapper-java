package saetabis.automation.testingWrapper.services.ui.elements;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegisterElements extends Elements{

    public RegisterElements(Integer _timeout, WebDriver _driver) {
        super(_timeout, _driver);
    }

    @FindBy(name="borrowerFirstName")
    public WebElement firsNameInput;

    @FindBy(name="borrowerLastName")
    public WebElement lastNameInput;

    @FindBy(name="borrowerStreet")
    public WebElement homeAdressInput;

    @FindBy(name="borrowerCity")
    public WebElement cityInput;

    @FindBy(name="borrowerState")
    public WebElement stateInput;

    @FindBy(name="borrowerZipCode")
    public WebElement zipCodeInput;

    @FindBy(name="borrowerDateOfBirth")
    public WebElement dateOfBirthInput;

    @FindBy(name="borrowerIncome")
    public WebElement annualIncomeInput;

    @FindBy(name="borrowerAdditionalIncome")
    public WebElement additionalIncomeInput;

    @FindBy(name="username")
    public WebElement emailInput;

    @FindBy(name="password")
    public WebElement passwordInput;

    @FindBy(xpath="//input[@name='agreements']/following-sibling::*[1]")
    public WebElement termAndConditionsCheckbox;

    @FindBy(xpath="//*[contains(text(),'Check Your Rate ')]")
    public WebElement checkYourRateButton;
}
