package saetabis.automation.testingWrapper.services.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;
import saetabis.automation.testingWrapper.config.PagesConfig;
import saetabis.automation.testingWrapper.models.UpgradeUser;
import saetabis.automation.testingWrapper.services.ui.elements.CustomElements.DropdownElement;
import saetabis.automation.testingWrapper.services.ui.elements.HomeElements;

public class HomePage extends Page {

    private HomeElements homeElements;

    public HomePage(WebDriver _driver, PagesConfig _pagesConfig) {
        super(_driver, _pagesConfig.getBaseUrl(), "");

        pagesConfig = _pagesConfig;

        homeElements = new HomeElements(30, driver);
    }

    public RegisterPage checkYourRate(UpgradeUser user) {

        Reporter.log("Entering loan amount of: " + user.getLoan().getAmount(),true);
        homeElements.getLoanAmmountInput().sendKeys(user.getLoan().getAmount().toString());

        Reporter.log("Selecting purpose: " + user.getLoan().getPurpose(),true);
        DropdownElement purposeDropdown = new DropdownElement(homeElements.getLoanPurposeDropdown());
        purposeDropdown.selectOption(user.getLoan().getPurpose());

        Reporter.log("Clicking check your rate button",true);
        homeElements.getCheckYourRateButton().click();

        return new RegisterPage(driver, pagesConfig);
    }
}