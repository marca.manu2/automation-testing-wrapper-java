package saetabis.automation.testingWrapper.services.ui.elements;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginElements extends Elements{

    public LoginElements(Integer _timeout, WebDriver _driver) {
        super(_timeout, _driver);
    }

    @FindBy(name="username")
    public WebElement usernameInput;

    @FindBy(name="password")
    public WebElement passwordInput;

    @FindBy(css="button[data-auto=\"login\"]")
    public WebElement signInButton;
}
