package saetabis.automation.testingWrapper.services.ui.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Reporter;
import saetabis.automation.testingWrapper.config.PagesConfig;
import saetabis.automation.testingWrapper.models.UpgradeUser;
import saetabis.automation.testingWrapper.services.ui.elements.CustomElements.AutoCompleteElement;
import saetabis.automation.testingWrapper.services.ui.elements.RegisterElements;

public class RegisterPage extends Page{

    private RegisterElements registerElements;

    public RegisterPage(WebDriver _driver, PagesConfig _pagesConfig) {
        super(_driver, _pagesConfig.getBaseUrl(), _pagesConfig.getRegisterPath());

        pagesConfig = _pagesConfig;
        registerElements = new RegisterElements(30, _driver);
    }

    public OfferPage registerUser(UpgradeUser user) {

        Reporter.log("REGISTERING USER",true);
        Reporter.log(dataReader.readObjToJsonString(user),true);

        fluentWait.until( ExpectedConditions.elementToBeClickable(registerElements.firsNameInput));

        registerElements.firsNameInput.sendKeys(user.getFirstName());
        registerElements.lastNameInput.sendKeys(user.getLastName());

        AutoCompleteElement homeAdressInput = new AutoCompleteElement(registerElements.homeAdressInput);
        homeAdressInput.choose(fluentWait, user.getAdress().getFullAdress());

        registerElements.cityInput.sendKeys(user.getAdress().getCity());
        registerElements.stateInput.sendKeys(user.getAdress().getState());
        registerElements.zipCodeInput.sendKeys(user.getAdress().getZipCode());
        registerElements.dateOfBirthInput.sendKeys(user.getDayOfBithday());
        registerElements.annualIncomeInput.sendKeys(user.getAnnualIncome().toString());
        registerElements.additionalIncomeInput.sendKeys(user.getAdditionalIncome().toString());

        registerElements.emailInput.sendKeys(user.getEmail());
        registerElements.passwordInput.sendKeys(user.getPassword());

        registerElements.termAndConditionsCheckbox.click();

        registerElements.checkYourRateButton.click();

        waitForLoading();

        return new OfferPage(driver, pagesConfig);
    }
}
