package saetabis.automation.testingWrapper.services.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Reporter;
import saetabis.automation.testingWrapper.config.PagesConfig;
import saetabis.automation.testingWrapper.services.ui.elements.OfferElements;

public class OfferPage extends Page {

    private OfferElements offerElements;

    public OfferPage(WebDriver _driver, PagesConfig _pagesConfig) {
        super(_driver, _pagesConfig.getBaseUrl(), _pagesConfig.getOfferPath());

        pagesConfig = _pagesConfig;
        offerElements = new OfferElements(30, _driver);

        fluentWait.until(ExpectedConditions.elementToBeClickable(offerElements.headerMenu));
    }

    public String getLoanAmmount() {

        return getValueFromElement(offerElements.loanAmount, "loan amount");
    }

    public String getAPR() {

        return getValueFromElement(offerElements.apr, "apr");
    }

    public String getLoanTerm() {

        return getValueFromElement(offerElements.loanTerm, "loan term");
    }

    public String getMonthlyPayment() {

        return getValueFromElement(offerElements.monthlyPayment, "monthly payment");
    }

    public LogoutPage logOff() {
        Reporter.log("Loggin off" ,true);
        offerElements.headerMenu.click();
        fluentWait.until(ExpectedConditions.elementToBeClickable(offerElements.logOutButton));
        offerElements.logOutButton.click();

        return new LogoutPage(driver, pagesConfig);
    }

    private String getValueFromElement(WebElement element, String value)
    {
        Reporter.log("Getting "+ value + " ..." ,true);
        fluentWait.until(ExpectedConditions.elementToBeClickable(element));

        String textValue = element.getText();
        Reporter.log(value + ": " + textValue ,true);
        return textValue;
    }
}
