package saetabis.automation.testingWrapper.services.ui.elements;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HeaderElements extends Elements{

    public HeaderElements(Integer _timeout, WebDriver _driver) {
        super(_timeout, _driver);
    }

    @FindBy(css=".header-nav")
    public WebElement headerMenu;

    @FindBy(css="a[href=\"/phone/logout\"]")
    public WebElement logOutButton;

}
