package saetabis.automation.testingWrapper.services.ui.elements;


import lombok.Data;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import saetabis.automation.testingWrapper.services.ui.elements.CustomElements.DropdownElement;

@Data
public class HomeElements extends Elements{

    public HomeElements(Integer _timeout, WebDriver _driver) {
        super(_timeout, _driver);
    }

    @FindBy(id="loan-amount")
    public WebElement loanAmmountInput;

    @FindBy(id="loan-purpose-select")
    public WebElement loanPurposeDropdown;

    @FindBy(id="loan-form-submit")
    public WebElement checkYourRateButton;

}
