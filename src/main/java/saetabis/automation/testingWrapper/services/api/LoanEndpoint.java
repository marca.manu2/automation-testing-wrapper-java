package saetabis.automation.testingWrapper.services.api;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.testng.Reporter;
import saetabis.automation.testingWrapper.config.EndpointsConfig;

import static io.restassured.RestAssured.given;

@Component
public class LoanEndpoint {

    @Autowired
    private EndpointsConfig endpointsConfig;

    public Response getStates(Integer expectedHTTPStatusCode) {

        Reporter.log("Making call to GET states endpoint", true);
        return given()
                    .baseUri(endpointsConfig.getCredifyUrl())
                    .port(Integer.valueOf(endpointsConfig.getPort()))
                    .basePath(endpointsConfig.getLoanEndpoint())
                .when()
                    .contentType(ContentType.JSON)
                    .get("/states")
                .then()
                    .statusCode(expectedHTTPStatusCode)
                    .extract().response();
    }
}
