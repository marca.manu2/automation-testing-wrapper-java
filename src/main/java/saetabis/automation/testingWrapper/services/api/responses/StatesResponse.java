package saetabis.automation.testingWrapper.services.api.responses;

import lombok.Data;

import java.util.List;

@Data
public class StatesResponse {

	private List<State> states;

}
