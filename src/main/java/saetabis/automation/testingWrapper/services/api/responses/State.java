package saetabis.automation.testingWrapper.services.api.responses;

import lombok.Data;

@Data
public class State {

    private Integer minAge;

    private String label;

    private String abbreviation;

    private Long minLoanAmount;
}
